<?php

// Function which prints "Hello I am Zura"
// function hello()
// {
//     echo "hello I am Zura";
// }
// hello();
// Function with argument
// function hello($name)
// {
//     return "Hello I am $name";
// }
// echo hello('zul') . '<br>';
// echo hello('zakry') . '<br>';


// Create sum of two functions
// function sum($a, $b)
// {
//     return $a + $b;
// }
// echo sum(2, 4);

// Create function to sum all numbers using ...$nums
// function sum(...$Numbers)
// {
//     $sum = 0;
//     foreach ($Numbers as $Number) {
//         $sum += $Number;
//     }
//     return $sum;
// }

// echo 'total ' .  sum(1, 2, 3, 2, 6) . '<br>';

// Arrow functions
function sum(...$Numbers)
{
    return array_reduce($Numbers, fn ($carry, $n) => $carry + $n);
}

echo 'total ' .  sum(1, 2, 3, 2, 6) . '<br>';
